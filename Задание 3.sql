~~~~~~~~~~~~
Задание 3
~~~~~~~~~~~~
1) Посчитать количество постов для пользователя с заданным ID;
~~~~~~~~~~~~
SELECT count(*) FROM posts WHERE author_id=1;
~~~~~~~~~~~~

2) Выбрать N опубликованных постов, отсортированных в порядке убывания даты создания; (N = 10)
~~~~~~~~~~~~
SELECT * FROM posts WHERE status_id=3 ORDER BY create_date DESC LIMIT 10;
~~~~~~~~~~~~

3) Выбрать N постов в статусе "ожидает публикации", отсортированных в порядке возрастания даты создания; (N = 10)
~~~~~~~~~~~~
SELECT * FROM posts WHERE status_id=2 ORDER BY create_date ASC LIMIT 10;
~~~~~~~~~~~~

4) Hайти N недавно обновленных постов определенного тэга для K страницы (в каждой странице L постов). (N = 10, K*N = 1)
~~~~~~~~~~~~
SELECT * FROM posts p INNER JOIN tags_to_posts ttp ON p.id = ttp.post_id INNER JOIN tags t ON t.id = ttp.tag_id WHERE t.id=1 AND p.change_date >= '2020-10-21' LIMIT 10 OFFSET 1;
~~~~~~~~~~~~

5)Найти N постов с наибольшим рейтингом за день/месяц/год. (N = 5)
~~~~~~~~~~~~
SELECT p.id, COUNT(pl.post_id) FROM posts p INNER JOIN post_likes pl ON p.id = pl.post_id GROUP BY p.id ORDER BY count DESC LIMIT 5;
~~~~~~~~~~~~

Задание 4
1)Найти N наиболее посещаемых постов за день/месяц/год. (N = 10, месяц)
~~~~~~~~~~~~
SELECT p.id, count(s.user_agent) AS count FROM posts p INNER JOIN statistics s ON p.id = s.post_id WHERE s.create_date >= (now() - interval '1 month') GROUP BY p.id ORDER BY count DESC  LIMIT 10;
~~~~~~~~~~~~

2)Найти N наиболее посещаемых постов для заданного пользователя за все время, которые создал не он, но которые он редактировал. (N = 10, id автора = 1)
~~~~~~~~~~~~
SELECT p.id, count(s.id) AS count FROM posts p INNER JOIN statistics s ON p.id = s.post_id INNER JOIN edits e ON p.id = e.post_id WHERE e.author_id = 1 AND p.author_id != 1 GROUP BY p.id ORDER BY count DESC LIMIT 10;
~~~~~~~~~~~~

3) Найти N пользователей, для которых суммарный рейтинг для всех созданных ими постов максимальный среди всех пользователей. (N = 10)
~~~~~~~~~~~~
SELECT u.id, count(pl.id) AS count FROM users u INNER JOIN posts p ON u.id=p.author_id INNER JOIN post_likes pl ON p.id = pl.post_id GROUP BY u.id ORDER BY count DESC LIMIT 10;
~~~~~~~~~~~~

4) Найти N пользователей, для которых суммарный рейтинг для всех созданных ими постов максимальный среди всех пользователей младше K лет.
~~~~~~~~~~~~
SELECT u.id AS userid, count(pl.id) AS likes_count FROM users u INNER JOIN posts p ON u.id=p.author_id INNER JOIN post_likes pl ON p.id = pl.post_id WHERE u.birthdate >= (now() - interval'10 years') GROUP BY u.id LIMIT 10;
~~~~~~~~~~~~

5) Найти N пользователей с наибольшим рейтингом. (N = 10)
~~~~~~~~~~~~
SELECT t1.id, (post_rating*50/100 + edit_rating*30/100 + comment_rating*20/100) as rating
FROM
	(SELECT u.id as id, sum(pl.quantity) as post_rating  
	FROM users u 
	INNER JOIN posts p ON u.id=p.author_id 
	INNER JOIN post_likes pl ON p.id=pl.post_id 
	GROUP BY u.id  ORDER BY u.id) t1
LEFT JOIN
	(SELECT u.id as id, sum(cl.quantity)as comment_rating  
	FROM users u 
	INNER JOIN posts p ON u.id=p.author_id 
	INNER JOIN comments c ON p.id=c.post_id 
	INNER JOIN comment_likes cl ON c.id=cl.comment_id 
	GROUP BY u.id  ORDER BY u.id) t2
ON (t1.id = t2.id)
LEFT JOIN
	(SELECT u.id, sum(epl.quantity)as edit_rating  
	FROM users u 
	INNER JOIN edits e ON u.id=e.author_id 
	INNER JOIN post_likes epl ON e.post_id=epl.post_id
	GROUP BY u.id  ORDER BY u.id) t3
ON (t1.id = t3.id) ORDER BY rating DESC LIMIT 10;
~~~~~~~~~~~~

6) Hайти N тэгов, для которых суммарное количество посещений связанных с ними постов наибольшее за неделю. (N = 10)
~~~~~~~~~~~~
SELECT t.id, t.name, count(s.id) FROM tags t INNER JOIN tags_to_posts ttp ON t.id=ttp.tag_id INNER JOIN posts p ON ttp.post_id=p.id INNER JOIN statistics s ON p.id=s.post_id GROUP BY t.id ORDER BY count DESC LIMIT 10;
~~~~~~~~~~~~



~~~~~~~~~~~~
Задание 6
~~~~~~~~~~~~
1) Копировать пост по id вместе со связанными авторами и тэгами, но без статистики, комментариев и рейтинга. Скопированный пост должен быть в статусе черновик.

~~~~~~~~~~~~

DO $$
  DECLARE last_id INT;
BEGIN
last_id := ((SELECT MAX(id) FROM posts)+1);
 INSERT INTO posts(id, title, text, status_id, author_id, create_date, change_date) SELECT last_id, title, text, 2, author_id, create_date, change_date FROM posts WHERE id=1;
 INSERT INTO tags_to_posts(id, tag_id, post_id) SELECT ((SELECT MAX(id) FROM tags_to_posts)+1), tag_id, last_id FROM tags_to_posts WHERE post_id=1;
 
 COMMIT;
END $$;
~~~~~~~~~~~~


2) Удалить всех пользователей, у который рейтинг меньше чем N , вместе со всеми постами и комментариями. Порядок удаления сущностей: комментарии к постам пользователя, комментарии пользователя, посты пользователя, пользователь.  (<2000)
Вообще тут просто нужно сделать DELETE CASCADE, но поскольку это последнее задание и в нём чётко описан порядок удаления, решил так тоже оставить:
~~~~~~~~~~~~

DO $$
	DECLARE
   elem INTEGER;
BEGIN
	FOR elem IN 
		SELECT t1.id 
		FROM
		(SELECT u.id as id, sum(pl.quantity) as post_rating  
		 FROM users u 
		 INNER JOIN posts p ON u.id=p.author_id 
		 INNER JOIN post_likes pl ON p.id=pl.post_id 
		 GROUP BY u.id  ORDER BY u.id) t1
		LEFT JOIN
		(SELECT u.id as id, sum(cl.quantity)as comment_rating  
		 FROM users u 
		 INNER JOIN posts p ON u.id=p.author_id 
		 INNER JOIN comments c ON p.id=c.post_id 
		 INNER JOIN comment_likes cl ON c.id=cl.comment_id 
		 GROUP BY u.id  ORDER BY u.id) t2
		ON (t1.id = t2.id)
		LEFT JOIN
		(SELECT u.id, sum(epl.quantity)as edit_rating  
		FROM users u 
		INNER JOIN edits e ON u.id=e.author_id 
		INNER JOIN post_likes epl ON e.post_id=epl.post_id
		GROUP BY u.id  ORDER BY u.id) t3
		ON (t1.id = t3.id) WHERE (post_rating*50/100 + edit_rating*30/100 + comment_rating*20/100)<=13

	LOOP
		
		DELETE FROM comment_likes WHERE comment_id IN (SELECT c.id FROM users u INNER JOIN posts p ON u.id=p.author_id INNER JOIN comments c ON p.id=c.post_id INNER JOIN comment_likes cl ON c.id=cl.comment_id WHERE u.id=elem);
	   	DELETE FROM comments WHERE post_id IN (SELECT p.id FROM users u INNER JOIN posts p ON u.id=p.author_id WHERE u.id=elem);
		DELETE FROM comments WHERE author_id = elem;
		DELETE FROM posts WHERE author_id = elem;
		DELETE FROM users WHERE id = elem;

   END LOOP;

COMMIT;
END $$;

~~~~~~~~~~~~
Добавить следующие столбцы к таблицам, используя alter table:
~~~~~~~~~~~~
3) Статус пользователей. Статус может иметь значения активен или заблокирован. Новый столбец должен быть заполнен значением активен.

BEGIN;
ALTER TABLE users ADD COLUMN status BOOLEAN;
ALTER TABLE users ALTER COLUMN status SET DEFAULT true;
COMMIT;

UPDATE users SET status=true WHERE (SELECT id FROM users WHERE status IS NULL);
ALTER TABLE users ALTER COLUMN status SET NOT NULL;
~~~
4) Даты создания и обновления для постов. Новые столбцы должны быть заполнены значением текущей даты.

BEGIN;
ALTER TABLE posts ADD COLUMN create_date DATE, ADD COLUMN change_date DATE;
ALTER TABLE posts ALTER COLUMN create_date SET DEFAULT now(), ALTER COLUMN change_date SET DEFAULT now();
COMMIT;

UPDATE posts SET create_date=now() WHERE (SELECT id FROM posts WHERE create_date IS NULL);
UPDATE posts SET change_date=now() WHERE (SELECT id FROM posts WHERE change_date IS NULL);
ALTER TABLE posts ALTER COLUMN create_date SET NOT NULL, ALTER COLUMN change_date SET NOT NULL;