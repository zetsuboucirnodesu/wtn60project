Задание 3.
2), 3)
	SELECT * FROM posts 
	WHERE status_id= (SELECT id FROM post_statuses WHERE name = 'опубликован') 
	ORDER BY create_date DESC 
	LIMIT 10;

5)
	SELECT p.id, COUNT(pl.post_id) 
	FROM posts p 
	INNER JOIN post_likes pl ON p.id = pl.post_id 
	WHERE EXTRACT(MONTH  FROM p.create_date) >= EXTRACT(MONTH FROM TO_DATE('October', 'Month'))
	GROUP BY p.id 
	ORDER BY count DESC 
	LIMIT 5;

Задание 4
1) Вообще я не удалил не-роллап по ошибке, но раз уж так вышло
	SELECT EXTRACT(YEAR FROM p.create_date) y, 
	EXTRACT(MONTH FROM p.create_date) m, 
	EXTRACT(DAY FROM p.create_date) d, 
	count(s.user_agent) AS count 
	FROM posts p 
	LEFT JOIN statistics s ON p.id = s.post_id 
	GROUP BY p.create_date 
	UNION
	SELECT EXTRACT(YEAR FROM p.create_date) y, 
	EXTRACT(MONTH FROM p.create_date) m, 
	NULL AS d,
	count(s.user_agent) AS count 
	FROM posts p 
	LEFT JOIN statistics s ON p.id = s.post_id 
	GROUP BY p.create_date 
	UNION
	SELECT EXTRACT(YEAR FROM p.create_date) y, 
	NULL AS m, 
	NULL AS d, 
	count(s.user_agent) AS count 
	FROM posts p 
	LEFT JOIN statistics s ON p.id = s.post_id 
	GROUP BY p.create_date 
	LIMIT 10;

Задание 6
	CREATE TABLE users_new AS TABLE users;

	CREATE OR REPLACE FUNCTION copy_upaft()
  	RETURNS trigger AS
	$BODY$
	BEGIN
	    UPDATE users_new
	    SET name = NEW.name,
	    surname = NEW.surname,
	    birthday = NEW.birthday,
	    email = NEW.email,
	    pass = NEW.pass,
	    adress = NEW.adress,
	    status = true
	    WHERE users_new.id = NEW.id;

	    RETURN NULL;
	END;
	$BODY$
	  LANGUAGE plpgsql;

	CREATE TRIGGER upaft
	  AFTER UPDATE
	  ON users
	  FOR EACH ROW
	  EXECUTE PROCEDURE copy_upaft();


	ALTER TABLE users_new ADD COLUMN status BOOLEAN;
	ALTER TABLE users_new ALTER COLUMN status SET DEFAULT true;

	ALTER TABLE users RENAME TO users_old;
	ALTER TABLE users_new RENAME TO users;
	ALTER TABLE users DISABLE TRIGGER upaft;