1. Индексы добавил

2. Да, нигде не сказано что пост должен быть varchar. Нужно использовать text, он занимает столько же дискового пространства сколько varchar 255+, но не может быть индексом (а ему и не надо)
ALTER TABLE posts ALTER COLUMN text TYPE TEXT;

3. Потому что лайки у нас не свойство поста, а отдельное отношение. Целесообразно держать их отдельно, чтобы не менять каждый раз таблицу с постами. Лайки скорее всего будут часто меняться.

Задание 3
2), 3) Не понял. Ну если нужно чтобы в запросе обязательно присутствовало "опубликован", можно сделать что-то вроде
x := 'опубликован';
CASE x
    WHEN "опубликован" THEN
        status_id := 3;
    WHEN "черновик" THEN
        status_id := 2;
    ELSE
        status_id := 1;
END CASE;
SELECT * FROM posts 
WHERE status_id=status_id 
ORDER BY create_date DESC 
LIMIT 10;



4) SELECT p.id, p.title, TO_CHAR(p.create_date :: DATE, 'dd/mm/yyyy'), TO_CHAR(p.change_date :: DATE, 'dd/mm/yyyy')  
FROM posts p 
INNER JOIN tags_to_posts ttp ON p.id = ttp.post_id 
INNER JOIN tags t ON t.id = ttp.tag_id 
WHERE t.id=1 
ORDER BY p.change_date DESC 
LIMIT N OFFSET (K-1)*L;  /* Потому что страница нужна текущая, а не следующая */

5) SELECT p.id, COUNT(pl.post_id) 
FROM posts p 
INNER JOIN post_likes pl ON p.id = pl.post_id 
WHERE p.create_date >= '2020-10-21' 
GROUP BY p.id 
ORDER BY count DESC 
LIMIT 5;

Задание 4

1) 
	SELECT TO_CHAR(p.create_date :: DATE, 'dd/mm/yyyy'), count(s.user_agent) AS count 
	FROM posts p 
	INNER JOIN statistics s ON p.id = s.post_id 
	GROUP BY p.create_date 
	ORDER BY count DESC 
	LIMIT 10;

	ROLLUP:
	SELECT EXTRACT(YEAR FROM p.create_date) y, 
	SELECT EXTRACT(MONTH FROM p.create_date) m, 
	SELECT EXTRACT(DAY FROM p.create_date) d, 
	count(s.user_agent) AS count 
	FROM posts p 
	INNER JOIN statistics s ON p.id = s.post_id 
	GROUP BY ROLLUP (
        EXTRACT (YEAR FROM p.create_date),
        EXTRACT (MONTH FROM p.create_date),
        EXTRACT (DAY FROM p.create_date)
    )
	ORDER BY count DESC 
	LIMIT 10;


2) 	
	SELECT p.id, count(t1.id) AS count 
	FROM posts p 
	LEFT JOIN (SELECT s.id, s.post_id, count(e.id) FROM statistics s 
	LEFT JOIN edits e 
	ON s.post_id = e.post_id 
	WHERE e.author_id = 1 
	GROUP BY s.id) t1
	ON t1.post_id=p.id
	WHERE p.author_id != 1  
	GROUP BY p.id 
	ORDER BY count 
	DESC LIMIT 10;

5) 
	SELECT t1.id, (post_rating*50/100 + edit_rating*30/100 + comment_rating*20/100) as rating
	FROM
		(SELECT p.author_id as id, sum(pl.quantity) as post_rating  
		FROM posts p  
		INNER JOIN post_likes pl ON p.id=pl.post_id 
		GROUP BY p.author_id) t1
	LEFT JOIN
		(SELECT p.author_id as id, sum(cl.quantity)as comment_rating  
		FROM posts p  
		INNER JOIN comments c ON p.id=c.post_id 
		INNER JOIN comment_likes cl ON c.id=cl.comment_id 
		GROUP BY p.author_id) t2
	ON (t1.id = t2.id)
	LEFT JOIN
		(SELECT e.author_id as id, sum(epl.quantity)as edit_rating  
		FROM edits e  
		INNER JOIN post_likes epl ON e.post_id=epl.post_id
		GROUP BY e.author_id) t3
	ON (t1.id = t3.id) WHERE (post_rating*50/100 + edit_rating*30/100 + comment_rating*20/100)>0 ORDER BY rating DESC LIMIT 10;

6) 
	SELECT t.id, t.name, count(post.sid) FROM tags t 
	LEFT JOIN tags_to_posts ttp ON t.id=ttp.tag_id 
	LEFT JOIN 
	(SELECT p.id as id, s.id as sid FROM posts p  
	LEFT JOIN statistics s ON p.id=s.post_id 
	WHERE p.create_date >= (now() - interval '1 week')) as post
	ON ttp.post_id=post.id
	GROUP BY t.id 
	ORDER BY count DESC 
	LIMIT 10;

Задание 6

1) DO $$
  	DECLARE last_id INT;
	DECLARE status INT;
	DECLARE x varchar;
BEGIN
x := 'черновик';
CASE x
    WHEN 'опубликован' THEN
        status := 3;
    WHEN 'черновик' THEN
        status := 2;
    ELSE
        status := 1;
END CASE;

 INSERT INTO posts(title, text, status_id, author_id, create_date, change_date) SELECT title, text, status, author_id, create_date, change_date FROM posts WHERE id=1 RETURNING id INTO last_id;
 INSERT INTO tags_to_posts(tag_id, post_id) SELECT tag_id, last_id FROM tags_to_posts WHERE post_id=1;
 
 COMMIT;
END $$;

2) DO $$
	DECLARE
   elem INTEGER;
BEGIN
	FOR elem IN 
		SELECT t1.id 
		FROM
		(SELECT p.author_id as id, sum(pl.quantity) AS post_rating  
		 FROM posts p 
		 INNER JOIN post_likes pl ON p.id=pl.post_id 
		 GROUP BY p.author_id) t1
		LEFT JOIN
		(SELECT p.author_id  as id, sum(cl.quantity) AS comment_rating  
		 FROM posts p 
		 INNER JOIN comments c ON p.id=c.post_id 
		 INNER JOIN comment_likes cl ON c.id=cl.comment_id 
		 GROUP BY p.author_id) t2
		ON (t1.id = t2.id)
		LEFT JOIN
		(SELECT e.author_id as id, sum(epl.quantity) AS edit_rating  
		FROM edits e 
		INNER JOIN post_likes epl ON e.post_id=epl.post_id
		GROUP BY e.author_id) t3
		ON (t1.id = t3.id) WHERE (post_rating*50/100 + edit_rating*30/100 + comment_rating*20/100)<=13

	LOOP
		
		DELETE FROM comment_likes WHERE comment_id IN (SELECT c.id FROM users u INNER JOIN posts p ON u.id=p.author_id INNER JOIN comments c ON p.id=c.post_id INNER JOIN comment_likes cl ON c.id=cl.comment_id WHERE u.id=elem);
	   	DELETE FROM comments WHERE post_id IN (SELECT p.id FROM users u INNER JOIN posts p ON u.id=p.author_id WHERE u.id=elem);
		DELETE FROM comments WHERE author_id = elem;
		DELETE FROM posts WHERE author_id = elem;
		DELETE FROM users WHERE id = elem;

   END LOOP;

COMMIT;
END $$;

3) 
DO $$
	DECLARE status_val BOOLEAN;
	DECLARE x varchar;
BEGIN
	x := 'активен';
	CASE x
	    WHEN 'активен' THEN
	        status_val := TRUE;
	    ELSE
	        status_val := FALSE;
	END CASE;

	ALTER TABLE users ADD COLUMN status BOOLEAN;
	ALTER TABLE users ALTER COLUMN status SET DEFAULT true;

	UPDATE users SET status=status_val;
	ALTER TABLE users ALTER COLUMN status SET NOT NULL;
COMMIT;
END $$;

4) 

BEGIN;
ALTER TABLE posts ADD COLUMN create_date DATE, ADD COLUMN change_date DATE;
ALTER TABLE posts ALTER COLUMN create_date SET DEFAULT now(), ALTER COLUMN change_date SET DEFAULT now();

UPDATE posts SET create_date=now() WHERE (SELECT id FROM posts WHERE create_date IS NULL);
UPDATE posts SET change_date=now() WHERE (SELECT id FROM posts WHERE change_date IS NULL);
ALTER TABLE posts ALTER COLUMN create_date SET NOT NULL, ALTER COLUMN change_date SET NOT NULL;
COMMIT;